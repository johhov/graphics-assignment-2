#pragma once

#include <vector>

#include "ScreenObject.h"
#include "ModelHandler.h"

//If this file includes level.h, then level.h cant include this file. Since the definition would be recursive
class Movable : public ScreenObject {
public:
	enum direction {
		UP,
		LEFT,
		DOWN,
		RIGHT,
		NONE
	};

	Movable() {};
	Movable(Model* model, int textureID, glm::vec3 position, glm::vec3 hitbox) : ScreenObject(model, textureID, position, hitbox) {};
	~Movable() {};

	void move(std::vector<ScreenObject>* objects, float deltaTime);

	bool handleCollision(std::vector<ScreenObject>* objects);
	bool checkCollisionWith(ScreenObject* collidable);
	void resolveCollision(std::vector<ScreenObject>* objects, ScreenObject* collidable);

	void setDirection(Movable::direction direction);
	void setSpeed(float speed);	
protected:
	direction m_direction = Movable::direction::NONE;
	direction m_oldDirection = Movable::direction::NONE;
	glm::vec3 m_movementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 m_oldMovementDirection = glm::vec3(0.0f, 0.0f, 0.0f);

	float m_speed = 10.0f;
};