#pragma	once

#include <string>
#include <vector>
#include <glm/glm.hpp>

#include "ScreenObject.h"
#include "Movable.h"
#include "Pacman.h"

class Level 
{
public:
	Level(std::string filepath, ModelHandler* mH, TextureHandler* tH);

	std::vector<ScreenObject>* getWalls();
	Pacman* getPacman() { return pacman; }
	glm::vec2 getMapSize();
private:
	std::vector<std::vector<int>> map;
	std::vector<ScreenObject> walls;
	Pacman* pacman;
	glm::vec2 mapSize;
	int tileSize;

	void loadMap(std::string filepath); //Loads map data from file
	void createWalls(ModelHandler* modelHandler, TextureHandler* textureHandler);	//This should create a ScreenObject for each wall segment.  
};