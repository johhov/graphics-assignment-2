#version 410
	

	const int maxLights = 10;	
	int currentLight = 0;
	
	uniform int lightCount;
	
	uniform vec3 lightDirection[maxLights];
	uniform float lightArc[maxLights];
	
	uniform vec3 lightPos[maxLights];
	uniform vec3 lightSpecular[maxLights];
	uniform vec3 lightDiffuse[maxLights];
	uniform vec3 lightAmbient[maxLights];
	
	uniform sampler2D shadowTexture[maxLights]; //Its really a depth map
	
	uniform vec3 surfaceSpecular, surfaceDiffuse, surfaceAmbient;
	uniform float specularExponent;
	
	uniform sampler2D textureBuffer;
	
	uniform mat4 ViewMatrix;
	
	in vec4 shadowST[maxLights];
	in vec3 posEye, normalEye;
	in vec2 fragmentTexCoord;		
	
	out vec4 fragment_colour;
	
	//since pointlights need 4 depthmaps (not using up/down) 
	//	we need to send 4 spotlights (360 arc to avoid "cones") for 1 pointlight
	//	thats why we cut them off at the x coordinates of the depth map
	// 	in calcShadow. So that the redudant lights wont ruin eachothers shadow.
	//	but we dont care about doing shadows in the up and down direction, so there 
	// 	are no depthmaps for those.
	
	float calcShadow (in vec4 texcoods) {
		// constant that you can use to slightly tweak the depth comparison
		float epsilon = 0.0001;
		if (lightArc[currentLight] < 3.0){ //spotlight
			if (texcoods.x > 1.0 || texcoods.x < 0.0 || texcoods.y > 1.0 || texcoods.y < 0.0 || texcoods.w < 0.0) {
				return 1.0; // do not add shadow/ignore
			}
		}else{	//pointlight
			if (texcoods.x > 1.0 || texcoods.x < 0.0) {
				return 0.0; //shadow
			}
		}
		float shadow = texture (shadowTexture[currentLight], texcoods.xy).r;
		
		if (shadow + epsilon < texcoods.z) {
			return 0.0; // shadowed
		}
		return 1.0; // not shadowed
	}
	
	void main () {
		vec4 texture = texture2D(textureBuffer, fragmentTexCoord);
		fragment_colour = texture;
		vec3 totalLight = vec3(0, 0, 0);
		
		for(int i = 0; i < lightCount; i++, currentLight = i){ //do light calculations for each light
			//Calculate direction factor
			vec3 lightPosEye = vec3(ViewMatrix * vec4(lightPos[currentLight], 1));
			vec3 directionToSurfaceEye = normalize(posEye - lightPosEye);
			vec3 lightDirectionEye = vec3(ViewMatrix * vec4(lightDirection[currentLight], 0));
			float directionDot = dot(directionToSurfaceEye, lightDirectionEye);
			float directionFactor = 1;
			if (directionDot < cos(lightArc[currentLight]))
				directionFactor = 0;

			//Ambient light
			vec3 ambient = surfaceAmbient;
			if(lightArc[currentLight] < 3.0){ //spotlight
				ambient *= lightAmbient[currentLight];
			}else{	//to account for redudant lights in pointlight
				ambient *= lightAmbient[currentLight] / 4;
			}
		
			//Diffuse light
			vec3 distanceToLightEye = lightPosEye - posEye;
			vec3 directionToLightEye = normalize(distanceToLightEye);
			float dotProd = dot(directionToLightEye, normalEye);
			dotProd = max(dotProd, 0.0);
			vec3 diffuse = lightDiffuse[currentLight] * surfaceDiffuse * dotProd  * directionFactor / max(1, distance(lightPosEye, posEye) / 5);
			
			//Specular light
			vec3 reflectionEye = reflect(-directionToLightEye, normalEye);
			vec3 surfaceToViewerEye = normalize(-posEye);
			float dotProdSpecular = dot(reflectionEye, surfaceToViewerEye);
			dotProdSpecular = max(dotProdSpecular, 0.0);
			float specularFactor = pow(dotProdSpecular, specularExponent);
			vec3 specular = lightSpecular[currentLight] * surfaceSpecular * specularFactor * directionFactor;
			
			//Shadows
			vec4 shadowCoord = shadowST[i];			
			//convert coordinates to texture coordinates
			shadowCoord.xyz /= shadowCoord.w;
			shadowCoord.xyz += 1.0;
			shadowCoord.xyz *= 0.5;		
			float shadowFactor = calcShadow(shadowCoord);
			
			//final light
			totalLight += ambient + (diffuse + specular) * shadowFactor;
		}
		//final colour
		fragment_colour *= vec4(totalLight, 1);
	}