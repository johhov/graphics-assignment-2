#version 410

	layout(location = 0) in vec3 vp;
	layout(location = 1) in vec2 texCoord;
	layout(location = 2) in vec3 normal;

	const int maxLights = 10; //Keep in mind that 1 pointlight = 4 spotlights (usually 6 but up and down arent needed for pacman)
	
	uniform mat4 ProjectionMatrix, ViewMatrix, ModelMatrix;

	uniform int lightCount;
	uniform mat4 lightVP[maxLights];
	
	out vec4 shadowST[maxLights];
	out vec3 posEye, normalEye;
	out vec2 fragmentTexCoord;

	void main () {
		posEye = vec3(ViewMatrix * ModelMatrix * vec4(vp, 1));
		normalEye = vec3(ViewMatrix * ModelMatrix * vec4(normal, 0));
		gl_Position = ProjectionMatrix * vec4(posEye, 1.0);	
		fragmentTexCoord = texCoord;
		
		//The position as viewed from the light
		//Used to work out shadow texture coordinates
		for(int i = 0; i < lightCount; i++){
			shadowST[i] = lightVP[i] * ModelMatrix * vec4(vp, 1);
		}
	};