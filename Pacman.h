#pragma once

#include "Movable.h"
#include "lightHandler.h"

class Pacman : public Movable {	
public:
	Pacman(Model* model, int textureID, glm::vec3 position, glm::vec3 hitbox) : Movable(model, textureID, position, hitbox) {};
	~Pacman();

	void setLight(glm::vec3 specular, glm::vec3 diffuse, glm::vec3 ambient);

	void changeLight();
	void update();
private:
	Light* lights[4]; //4 lights for a full pointlight (up and down arent needed)
	bool pointLight = true;
	direction lightDirection;
};

