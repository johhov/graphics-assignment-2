#pragma	once

#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <glm/glm.hpp>
#include <string>
#include <map>
#include <vector>

struct Model
{
	GLuint VAO, VBO, uvBO, normalBO, numVertices;
};

class ModelHandler {
public:
	ModelHandler() {};
	~ModelHandler() {};

	void init();
	void loadOBJ(std::string name, std::string path);
	Model* getModel(std::string name);

private:
	std::map<std::string, Model> m_models;
	
	Model createModel(std::vector<glm::vec3> vertices, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals);
};