#pragma	once

#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.h"
#include "ModelHandler.h"
#include "TextureHandler.h"
#include "ShaderHandler.h"

struct Material {
	glm::vec3 specular, diffuse, ambient;

	GLfloat specularExponent;

	Material() {
		specular = diffuse = ambient = glm::vec3(1, 1, 1);
		specularExponent = 10.0f;
	};

	Material(glm::vec3 s, glm::vec3 d, glm::vec3 a, float se) {
		specular = s; diffuse = d; ambient = a;
		specularExponent = se;
	}
};

class ScreenObject 
{
public:
	ScreenObject() {};
	ScreenObject(Model* model, GLuint texture, glm::vec3 position, glm::vec3 hitbox = glm::vec3(0));
	~ScreenObject() {};

	void draw(ShaderHandler::ShaderProgram* shaderProgram);
	void renderShadows(ShaderHandler::ShaderProgram* shaderProgram);
	virtual void update();

	glm::mat4 getModelMatrix();
	glm::vec3 getPosition();
	glm::vec3 getHitbox();

	void setTexture(int id);
	void setModel(Model* model);
	void setModelMatrix(glm::mat4 modelMatrix);
	void setScale(glm::vec3 scale);

protected:
	SDL_Rect	m_collisionRect;
	glm::mat4 	m_modelMatrix;
	glm::vec3	m_position,
				m_hitbox;
	Material	m_material;
	int			m_textureID;

private:
	Model* 		m_model;
	
};