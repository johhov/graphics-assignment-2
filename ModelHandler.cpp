#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "ModelHandler.h"

void ModelHandler::init()
{
	loadOBJ("cube", "./resources/models/cube.obj");
	loadOBJ("sphere", "./resources/models/sphere.obj");
	loadOBJ("plane", "./resources/models/plane.obj");
}

// Reads model from file
void ModelHandler::loadOBJ(std::string name, std::string path)
{
	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices, temp_normals;
	std::vector<glm::vec2> temp_uvs;

	FILE* file = fopen(path.c_str(), "r");
	if (file == nullptr)
	{
		printf("Cannot open file \"%s\"\n", path);
		return;
	}

	char lineHead[128];
	while (fscanf(file, "%s", lineHead) != EOF)		// Read in data from file:
	{
		if (!strcmp(lineHead,"v"))			// Add vertex:
		{
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (!strcmp(lineHead, "vt"))	// Add uv:
		{
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			temp_uvs.push_back(uv);
		}
		else if (!strcmp(lineHead, "vn"))	// Add normal:
		{
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (!strcmp(lineHead, "f"))	// Get indices:
		{
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", 
				&vertexIndex[0], &uvIndex[0], &normalIndex[0], 
				&vertexIndex[1], &uvIndex[1], &normalIndex[1], 
				&vertexIndex[2], &uvIndex[2], &normalIndex[2]);

			for (auto i = 0; i < 3; ++i)
			{
				vertexIndices.push_back(vertexIndex[i]);
				uvIndices.push_back(uvIndex[i]);
				normalIndices.push_back(normalIndex[i]);
			}
		}
	}

	std::vector<glm::vec3> out_vertices, out_normals;
	std::vector<glm::vec2> out_uvs;

	// Process vertex-data into buffers based on indexing:
	for (auto i = 0; i < vertexIndices.size(); ++i) 
	{
		out_vertices.push_back	(temp_vertices[vertexIndices[i] - 1]);
		out_uvs.push_back		(temp_uvs[uvIndices[i] - 1]);
		out_normals.push_back	(temp_normals[normalIndices[i] - 1]);
	}

	Model model = createModel(out_vertices, out_uvs, out_normals);
	
	if (!m_models.insert(std::pair<std::string, Model>(name, model)).second) 
		printf("ERROR: A model by the name of %s already exists.\n", name.c_str());

	fclose(file);
	return;
}


// Creates a model
Model ModelHandler::createModel(std::vector<glm::vec3> vertices, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals) {
	Model model;
	model.numVertices = vertices.size();

	glGenVertexArrays(1, &model.VAO);
	glBindVertexArray(model.VAO);

	glEnableVertexAttribArray(0);	// Vertex Positions
	glEnableVertexAttribArray(1);	// Vertex Texture Coords
	glEnableVertexAttribArray(2);	// Vertex Normals

	glGenBuffers(1, &model.VBO);
	glBindBuffer(GL_ARRAY_BUFFER, model.VBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &model.uvBO);
	glBindBuffer(GL_ARRAY_BUFFER, model.uvBO);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glGenBuffers(1, &model.normalBO);
	glBindBuffer(GL_ARRAY_BUFFER, model.normalBO);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glBindVertexArray(0);

	return model;
}


Model* ModelHandler::getModel(std::string name) 
{
	if(m_models.count(name)) 
		return &m_models[name];
	
	printf("ERROR: No model with the name \"%s\" could be found.\n", name.c_str());
	return nullptr;
}