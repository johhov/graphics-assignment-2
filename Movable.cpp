#include "Movable.h"


/**
* [Movable::move description]
* @param collidables [description]
*/
void Movable::move(std::vector<ScreenObject>* objects, float deltaTime) {
	m_position += m_movementDirection * m_speed * deltaTime;
	handleCollision(objects);
	m_modelMatrix = glm::translate(glm::mat4(1), m_position) * glm::scale(glm::mat4(1.0f), glm::vec3(1.0f)); //Need to update modelMatrix
}

/**
* This should rotate the object so that it faces in a specific direction.
* This will be usefull with the spotlight.
* @param direction The new direction
*/
//void Movable::rotate(glm::vec3 direction) {
//	//We didn't need this function, since pacman is a sphere
	//	spotlight direction just points to m_direction 
//}

bool Movable::handleCollision(std::vector<ScreenObject>* objects) {
	bool collision = false;

	for (auto collidable : (*objects)) {
		if (checkCollisionWith(&collidable)) {
			resolveCollision(objects, &collidable);
			collision = true;
		}
	}

	if (collision) {
		setDirection(m_oldDirection);
	}

	m_oldDirection = Movable::direction::NONE;
	m_oldMovementDirection = glm::vec3(0.0f, 0.0f, 0.0f);

	return collision;
}

bool Movable::checkCollisionWith(ScreenObject* collidable) {
	glm::vec3 collidablePos = collidable->getPosition();
	glm::vec3 collidableBox = collidable->getHitbox();

	if ((m_position.x + m_hitbox.x / 2 <= collidablePos.x - collidableBox.x / 2 || m_position.x - m_hitbox.x / 2 >= collidablePos.x + collidableBox.x / 2) ||
		(m_position.y + m_hitbox.y / 2 <= collidablePos.y - collidableBox.y / 2 || m_position.y - m_hitbox.y / 2 >= collidablePos.y + collidableBox.y / 2))
	{
		return false;
	}

	return true;
}

void Movable::resolveCollision(std::vector<ScreenObject>* objects, ScreenObject* collidable) {
	glm::vec3 collidablePos = collidable->getPosition();
	glm::vec3 collidableBox = collidable->getHitbox();

	glm::vec3 newPosition = m_position;

	float col;
	float thi;

	switch (m_direction) {
	case(UP) :
		col = (collidablePos.y - collidableBox.y / 2);
		thi = (m_position.y + m_hitbox.y / 2);
		newPosition.y = (int)(m_position.y - (thi - col));
		break;
	case(LEFT) :
		col = (collidablePos.x + collidableBox.x / 2);
		thi = (m_position.x - m_hitbox.x / 2);
		newPosition.x = (int)(m_position.x + (col - thi));
		break;
	case(DOWN) :
		col = (collidablePos.y + collidableBox.y / 2);
		thi = (m_position.y - m_hitbox.y / 2);
		newPosition.y = (int)(m_position.y + (col - thi));
		break;
	case(RIGHT) :
		col = (collidablePos.x - collidableBox.x / 2);
		thi = (m_position.x + m_hitbox.x / 2);
		newPosition.x = (int)(m_position.x - (thi - col));
		break;
	case(NONE) :
		printf("Error!: No direction during collision. This should not happen.\n");
		break;
	default:
		printf("Error!: The direction is not valid.\n");
	}

	m_position = newPosition;
}

void Movable::setDirection(Movable::direction direction) {
	m_oldMovementDirection = m_movementDirection;
	m_oldDirection = m_direction;

	switch (direction) {
	case(UP) :
		m_movementDirection = glm::vec3(0.0f, 1.0f, 0.0f);
		m_direction = direction;
		break;
	case(LEFT) :
		m_movementDirection = glm::vec3(-1.0f, 0.0f, 0.0f);
		m_direction = direction;
		break;
	case(DOWN) :
		m_movementDirection = glm::vec3(0.0f, -1.0f, 0.0f);
		m_direction = direction;
		break;
	case(RIGHT) :
		m_movementDirection = glm::vec3(1.0f, 0.0f, 0.0f);
		m_direction = direction;
		break;
	case(NONE) :
		m_movementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
		m_direction = direction;
		break;
	default:
		printf("Error!: The direction is not valid.\n");
	}
}

void Movable::setSpeed(float speed) {
	m_speed = speed;
}
