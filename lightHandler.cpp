#include "lightHandler.h"

//returns nullptr if unsuccessful
Light* LightHandler::createLight() {
	if (lightCount < maxLights) {
		lights[lightCount] = new Light();
		lights[lightCount]->initShadow(lightCount + textureCount);
		return lights[lightCount++];
	} else
		return nullptr; //to avoid crashing (atleast in this function)
}

void LightHandler::init(int tc) {
	textureCount = tc;
	lightCount = 0;
	sceneLightCount = 6;
	sceneLightRadius = 45.0f;
	sceneLightSpeed = 0.75f;

	for (int i = 0; i < sceneLightCount; i++) 
		createLight();
}

void LightHandler::toggleLights() {
	for (int i = 0; i < sceneLightCount; i++) lights[i]->active = !lights[i]->active;
}

void LightHandler::updateSceneLights(float dt) {
	if (fancyAngle > glm::two_pi<float>()) fancyAngle = 0;

	fancyAngle += dt * sceneLightSpeed;
	sceneLightHeight = 10.0f + 15.0f * cosf(fancyAngle) * cosf(fancyAngle);
	for (int i = 0; i < sceneLightCount; i++) {
		*lights[i]->pos = glm::vec3(sceneLightRadius * cosf(fancyAngle + i * glm::two_pi<float>() / sceneLightCount),	//x
									sceneLightRadius * sinf(fancyAngle + i * glm::two_pi<float>() / sceneLightCount),	//y
									sceneLightHeight);																	//z
		*lights[i]->direction = -*lights[i]->pos;
	}
}

void LightHandler::sendUniformData(ShaderHandler::ShaderProgram* shaderProgram) {
	int activeLightCount = 0;

	glm::vec3 pos[maxLights];
	glm::vec3 direction[maxLights];
	float arc[maxLights];
	glm::vec3 specular[maxLights];
	glm::vec3 diffuse[maxLights];
	glm::vec3 ambient[maxLights];
	GLuint depthFBTex[maxLights];
	glm::mat4 lightVP[maxLights];
	//a lot of these values don't change in between frames, but this way any value could be dynamic if desired.
	for (int i = 0; i < lightCount; i++) { //Build the arrays that we send to GLSL
		if (lights[i]->active) {
			pos[activeLightCount] = *lights[i]->pos;
			direction[activeLightCount] = *lights[i]->direction;
			specular[activeLightCount] = lights[i]->specular;
			diffuse[activeLightCount] = lights[i]->diffuse;
			ambient[activeLightCount] = lights[i]->ambient;
			depthFBTex[activeLightCount] = lights[i]->depthFBTex;
			arc[activeLightCount] = lights[i]->arc;
			lightVP[activeLightCount] = lights[i]->getProj() * lights[i]->getView();
			activeLightCount++;
		}		
	}
	glUniform1i(shaderProgram->lightCountId, activeLightCount);

	glUniform3fv(shaderProgram->lightDirectionId, activeLightCount, glm::value_ptr(direction[0]));
	glUniform3fv(shaderProgram->lightPositionId, activeLightCount, glm::value_ptr(pos[0]));
	glUniform1fv(shaderProgram->lightArcId, activeLightCount, &arc[0]);
	glUniform3fv(shaderProgram->lightSpecularId, activeLightCount, glm::value_ptr(specular[0]));
	glUniform3fv(shaderProgram->lightDiffuseId, activeLightCount, glm::value_ptr(diffuse[0]));
	glUniform3fv(shaderProgram->lightAmbientId, activeLightCount, glm::value_ptr(ambient[0]));

	glUniformMatrix4fv(shaderProgram->lightVPId, activeLightCount, GL_FALSE, glm::value_ptr(lightVP[0]));
	glUniform1iv(shaderProgram->shadowTexId, activeLightCount, (GLint*)&depthFBTex[0]);
}