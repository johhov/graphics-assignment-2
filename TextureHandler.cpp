#include "TextureHandler.h"

void TextureHandler::init()
{
	loadTexture("floor", "./resources/textures/floorTexTemp.png");
	loadTexture("wall", "./resources/textures/wallTexTemp.png");
	loadTexture("pacman", "./resources/textures/pacman.png");
}

void TextureHandler::loadTexture(const std::string& name, const std::string& path)
{
	// SDL Image is used to load the image and save it as a surface.
	SDL_Surface* textureSurface = IMG_Load(path.c_str());
	if (!textureSurface)
	{
		printf("Failed to load texture \"%s\": %s\n", path.c_str(), IMG_GetError());
		return;
	}

	// Generate and bind the texture buffer
	GLuint textureBuffer = 0;
	glActiveTexture(GL_TEXTURE0 + m_textureIDs.size()); 
	glGenTextures(1, &textureBuffer);
	glBindTexture(GL_TEXTURE_2D, textureBuffer);

	// We tell OpenGL what to do when it needs to render the texture at a higher or lower resolution.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// Create the texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureSurface->w, textureSurface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureSurface->pixels);

	SDL_FreeSurface(textureSurface);

	m_textureIDs.insert(std::pair<std::string, int>(name, m_textureIDs.size()));
}

int TextureHandler::getTexture(const std::string& name)
{
	if (m_textureIDs.count(name))
		return m_textureIDs[name];

	printf("ERROR: No texture with the name \"%s\" could be found.\n", name.c_str());
	return 0;
}

