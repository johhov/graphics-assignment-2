#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "Level.h"
#include "WindowHandler.h"

Level::Level(std::string filepath, ModelHandler* mH, TextureHandler* tH)
{
	loadMap(filepath);
	createWalls(mH, tH);
}

/* 
Reads a text file containing map data in the following format:
5x5
1 1 1 1 1
1 0 0 0 1
2 0 1 0 0
1 0 0 0 1
1 1 1 1 1
The first line defines the dimensions of the map
The following matrix has 1s representing walls and 0s representing open space.
2 represents Pac Mans start position.
*/
void Level::loadMap(std::string filepath) 
{
	int x, y, temp;
	FILE* file = fopen(filepath.c_str(), "r");
	
	fscanf(file, "%dx%d", &x, &y);
	mapSize = glm::vec2(x, y);

	for(int i = 0; i<mapSize.y; i++)
	{
		std::vector<int> row;
		for(int j = 0; j<mapSize.x; j++)
		{
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createWalls(ModelHandler* modelHandler, TextureHandler* textureHandler)
{
	glm::vec2 pos;

	Model* wallModel = modelHandler->getModel("cube");
	int wallTexture = textureHandler->getTexture("wall");
	
	tileSize = 2;

	for(int i = 0; i<mapSize.y; i++) 
	{

		for(int j = 0; j<mapSize.x; j++) 
		{
			if(map[i][j] == 1) 
			{
				ScreenObject wall(wallModel, wallTexture, { (j - (mapSize.x / 2)) * tileSize, (i - (mapSize.y / 2)) * tileSize, 0 }, glm::vec3(tileSize));
				walls.push_back(wall);

			}
			else if (map[i][j] == 2)
			{
				pacman = new Pacman(modelHandler->getModel("sphere"), textureHandler->getTexture("pacman"), { (j - (mapSize.x / 2)) * tileSize, (i - (mapSize.y / 2)) * tileSize, 0 }, glm::vec3(tileSize));
			}
		}
	}

	ScreenObject floor(modelHandler->getModel("plane"), textureHandler->getTexture("floor"), { -1, -1, -1 });
	floor.setScale({ mapSize.x, mapSize.y, 1 });
	walls.push_back(floor);
}

std::vector<ScreenObject>* Level::getWalls() 
{
	return &walls;
}

glm::vec2 Level::getMapSize()
{
	return mapSize;
}