#include "ScreenObject.h"
#include <stdio.h>

ScreenObject::ScreenObject(Model* model, GLuint textureID, glm::vec3 position, glm::vec3 hitbox)
	: m_model(model), m_textureID(textureID), m_position(position), m_hitbox(hitbox)
{
	m_modelMatrix = glm::translate(glm::mat4(1), position) * glm::scale(glm::mat4(1.0f), glm::vec3(1.0f));
	m_material = Material();
}

void ScreenObject::draw(ShaderHandler::ShaderProgram* shaderProgram) 
{	
	
	glUniformMatrix4fv(shaderProgram->modelMatrixId, 1, GL_FALSE, glm::value_ptr(m_modelMatrix));

	glUniform1fv(shaderProgram->surfaceSpecularExponentId, 1, (const GLfloat*)(&m_material.specularExponent));
	glUniform3fv(shaderProgram->surfaceSpecularId, 1, glm::value_ptr(m_material.specular));
	glUniform3fv(shaderProgram->surfaceDiffuseId, 1, glm::value_ptr(m_material.diffuse));
	glUniform3fv(shaderProgram->surfaceAmbientId, 1, glm::value_ptr(m_material.ambient));

	glUniform1i(shaderProgram->textureId, m_textureID);
	
	glBindVertexArray(m_model->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_model->VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_model->uvBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_model->normalBO);

	glDrawArrays(GL_TRIANGLES, 0, m_model->numVertices);
	
}

void ScreenObject::renderShadows(ShaderHandler::ShaderProgram* shaderProgram) {
	glUniformMatrix4fv(shaderProgram->modelMatrixId, 1, GL_FALSE, glm::value_ptr(m_modelMatrix));

	glBindVertexArray(m_model->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_model->VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_model->uvBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_model->normalBO);

	glDrawArrays(GL_TRIANGLES, 0, m_model->numVertices);
}

void ScreenObject::update() 
{

}

//GET/////////////////////////////////////////


glm::mat4 ScreenObject::getModelMatrix() 
{
	return m_modelMatrix;
}

glm::vec3 ScreenObject::getPosition()
{
	return m_position;
}
glm::vec3 ScreenObject::getHitbox()
{
	return m_hitbox;
}

//SET/////////////////////////////////////////
void ScreenObject::setTexture(int id)
{
	m_textureID = id;
}


void ScreenObject::setModel(Model* model) 
{
	m_model = model;
}

void ScreenObject::setModelMatrix(glm::mat4 modelMatrix) 
{
	m_modelMatrix = modelMatrix;
}

void ScreenObject::setScale(glm::vec3 scale)
{
	m_modelMatrix = glm::scale(m_modelMatrix, scale);
}
