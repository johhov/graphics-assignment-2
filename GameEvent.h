#pragma once

//Add new events here and in InputHandler::init()
enum class ActionEnum :int {
	NOACTION = 0,
	MOUSEMOTION,
	FREE_CAM_TOGGLE,
	PLAYER_MOVE_UP,
	PLAYER_MOVE_DOWN,
	PLAYER_MOVE_LEFT,
	PLAYER_MOVE_RIGHT,
	CHANGE_LIGHT,
	TOGGLE_SCENE_LIGHTS,
    QUIT,
    NUMBER_OF_GAME_EVENTS
};

/* This struct deals with an agent creating events.  This has an agent number and the action*/
struct GameEvent {
    int agent;
    ActionEnum action;
};