#include "ShaderHandler.h"

const std::string shaderPath = "./resources/shaders/";

ShaderHandler::ShaderProgram* ShaderHandler::initializeShaders()
{
	this->shaders["Red"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("Red"));
	this->shaders["phong"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("phong"));
	this->shaders["shadow"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("shadow"));

	return this->shaders["phong"].get(); 
}

ShaderHandler::ShaderProgram* ShaderHandler::getShader(const std::string& shader)
{
	auto it = this->shaders.find(shader);
	if (it != this->shaders.end())
	{
		return this->shaders[shader].get();
	}

	return nullptr;
}

ShaderHandler::ShaderProgram::ShaderProgram(const std::string& shader)
{
	const std::string vertexSuffix = ".vs";
	const std::string fragmentSuffix = ".fs";

	this->programId = LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str());
	
	this->MVPId = glGetUniformLocation(this->programId, "MVP");
	this->projectionMatrixId = glGetUniformLocation(this->programId, "ProjectionMatrix");
	this->viewMatrixId = glGetUniformLocation(this->programId, "ViewMatrix");
	this->modelMatrixId = glGetUniformLocation(this->programId, "ModelMatrix");

	this->textureId = glGetUniformLocation(this->programId, "textureBuffer");

	this->lightCountId = glGetUniformLocation(this->programId, "lightCount");

	this->lightDirectionId = glGetUniformLocation(this->programId, "lightDirection");
	this->lightArcId = glGetUniformLocation(this->programId, "lightArc");

	this->lightPositionId = glGetUniformLocation(this->programId, "lightPos");
	this->lightSpecularId = glGetUniformLocation(this->programId, "lightSpecular");
	this->lightDiffuseId = glGetUniformLocation(this->programId, "lightDiffuse");
	this->lightAmbientId = glGetUniformLocation(this->programId, "lightAmbient");

	this->surfaceSpecularExponentId = glGetUniformLocation(this->programId, "specularExponent");
	this->surfaceSpecularId = glGetUniformLocation(this->programId, "surfaceSpecular");
	this->surfaceDiffuseId = glGetUniformLocation(this->programId, "surfaceDiffuse");
	this->surfaceAmbientId = glGetUniformLocation(this->programId, "surfaceAmbient");

	this->cameraPositionId = glGetUniformLocation(this->programId, "CameraPosition_worldspace");

	//For shadows
	this->lightVPId = glGetUniformLocation(this->programId, "lightVP");
	this->shadowTexId = glGetUniformLocation(this->programId, "shadowTexture");

	glUseProgram(this->programId);
}

ShaderHandler::ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(this->programId);
}