//////////////////////////////////////////////////////////////
//Extra features:											//
//Free cam													//
//Obj file loader											//
//Multiple lights support									//
//Shadows													//
//////////////////////////////////////////////////////////////
//Controls:													//
//pacman wasd to move, e to toggle light					//
//L to toggle scene lights									//
//F to toggle freecam, wasd space, c and mouse to control	//
//////////////////////////////////////////////////////////////


#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <stdio.h>
#include <string>

#include "globals.h"

#include "InputHandler.h"
#include "WindowHandler.h"
#include "ShaderHandler.h"
#include "ModelHandler.h"
#include "TextureHandler.h"
#include "lightHandler.h"

#include "Level.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Movable.h"
#include "Pacman.h"
#include "Camera.h"


//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels(ModelHandler* mH, TextureHandler* tH) {
	FILE* file = fopen(gLEVELS_FILE, "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++) {
		char tempCString[51];
		fscanf(file, "%50s", &tempCString);
		tempString = tempCString;
		Level level(tempString, mH, tH);
		gLevels.push_back(level);
	}

	fclose(file);
	file = nullptr;
	
	return &gLevels.front();
}

bool initGL()
{
	//Success flag
	bool success = true;
	//glEnable(GL_CULL_FACE);
	//glFrontFace(GL_CCW);
	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	return success;
}

/**
 * Initializes the InputHandler, WindowHandler, and OpenGL
 */
bool init() {
	InputHandler::getInstance().init();
	

	if(!WindowHandler::getInstance().init()) {
		gRunning = false;
		return false;
	}

	initGL();

	return true;
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue, Camera &camera, Level* currentLevel) {
	GameEvent nextEvent;
	Pacman* pacman = currentLevel->getPacman();

	while(!eventQueue.empty()) {
		nextEvent = eventQueue.front();
		eventQueue.pop();
		if (SDL_ShowCursor(SDL_QUERY)) { //If not in free cam
			if (nextEvent.action == ActionEnum::PLAYER_MOVE_UP) pacman->setDirection(Movable::UP);
			if (nextEvent.action == ActionEnum::PLAYER_MOVE_DOWN) pacman->setDirection(Movable::DOWN);
			if (nextEvent.action == ActionEnum::PLAYER_MOVE_RIGHT) pacman->setDirection(Movable::RIGHT);
			if (nextEvent.action == ActionEnum::PLAYER_MOVE_LEFT) pacman->setDirection(Movable::LEFT);
		} else {
			if (nextEvent.action == ActionEnum::MOUSEMOTION)
				camera.updateAngle(InputHandler::getInstance().getMouseDelta());
		}

		if (nextEvent.action == ActionEnum::CHANGE_LIGHT) pacman->changeLight();

		if (nextEvent.action == ActionEnum::TOGGLE_SCENE_LIGHTS) LightHandler::getInstance().toggleLights();

		if (nextEvent.action == ActionEnum::FREE_CAM_TOGGLE) {
			SDL_ShowCursor(!SDL_ShowCursor(SDL_QUERY));
			SDL_SetRelativeMouseMode((SDL_bool)!(bool)SDL_GetRelativeMouseMode());	//The ! operator isnt supported by the SDL_bool it seems
		}		

		
	}
	 

	if (SDL_ShowCursor(SDL_QUERY))
		camera.lookAt(glm::vec3(0, 60 * cosf(glm::radians(120.0f)), 60 * sinf(glm::radians(120.0f))));
	else {
		camera.updatePos(InputHandler::getInstance().getKeyboardState(), deltaTime);
		deltaTime = 0;	//To pause the game while in free cam
	}
	pacman->update();
	pacman->move(currentLevel->getWalls(), deltaTime);	

	LightHandler::getInstance().updateSceneLights(deltaTime);
}

//All draw calls should originate here
void draw(Level* currentLevel, Camera* camera, ShaderHandler* shaderHandler) {
	ShaderHandler::ShaderProgram* shaderProgram = nullptr;
	//===========================================Render shadows to texture===========================================
	shaderProgram = shaderHandler->getShader("shadow");
	glUseProgram(shaderProgram->programId);

	for (int i = 0; i < LightHandler::getInstance().getLightCount(); i++) {
		Light* light = LightHandler::getInstance().getLight(i);
			if (light->active) {
				glBindFramebuffer(GL_FRAMEBUFFER, light->depthFB);
				glViewport(0, 0, light->shadowRes, light->shadowRes);
				glClearColor(0.0, 0.0, 0.0, 1.0);
				glClear(GL_DEPTH_BUFFER_BIT);

				glUniformMatrix4fv(shaderProgram->lightVPId, 1, GL_FALSE, glm::value_ptr(light->getProj() * light->getView()));

				//Draw level
				for (auto i : *currentLevel->getWalls())
					i.renderShadows(shaderProgram);
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(NULL);
	//===========================================Render scene===========================================
	shaderProgram = shaderHandler->getShader("phong");
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, WindowHandler::getInstance().getScreenSize().x, WindowHandler::getInstance().getScreenSize().y);

	glUseProgram(shaderProgram->programId);

	LightHandler::getInstance().sendUniformData(shaderProgram);

	glUniformMatrix4fv(shaderProgram->projectionMatrixId, 1, GL_FALSE, glm::value_ptr(camera->getProjectionMatrix()));
	glUniformMatrix4fv(shaderProgram->viewMatrixId, 1, GL_FALSE, glm::value_ptr(camera->getViewMatrix()));

	//Draw level
	for (auto i: *currentLevel->getWalls())
		i.draw(shaderProgram);
	//Draw pacman
	currentLevel->getPacman()->draw(shaderProgram);

	glUseProgram(NULL);
	//Update screen
	SDL_GL_SwapWindow(WindowHandler::getInstance().getWindow());
}

//Calls cleanup code on program exit.
void close() {
	WindowHandler::getInstance().close();
}

#undef main
int main(int argc, char *argv[]) {
	float nextFrame      = 1/gFpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime      = 0.0f; //Time since last pass through of the game loop.
	auto clockStart      = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop       = clockStart;

	

	init();

	std::queue<GameEvent> eventQueue; //Main event queue for the program.
	
	Camera mainCamera(glm::vec3(0, 60 * cosf(glm::radians(120.0f)), 60 * sinf(glm::radians(120.0f))));
	
	ModelHandler modelHandler;
	modelHandler.init();

	TextureHandler textureHandler;
	textureHandler.init();

	LightHandler::getInstance().init(textureHandler.getTextureCount());

	ShaderHandler shaderHandler;
	shaderHandler.initializeShaders();

	Level* currentLevel = loadLevels(&modelHandler, &textureHandler);
	currentLevel->getPacman()->setSpeed(10);
	currentLevel->getPacman()->setLight({ 1, 1, 0 }, { 1, 1, 0 }, { 0.002f, 0.002f, 0.002f });

	while(gRunning) {
		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		update(deltaTime, eventQueue, mainCamera, currentLevel);

		if(nextFrameTimer >= nextFrame) {
			draw(currentLevel, &mainCamera, &shaderHandler);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		
		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}