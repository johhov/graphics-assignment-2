#include "Pacman.h"

Pacman::~Pacman() {
}

void Pacman::setLight(glm::vec3 specular, glm::vec3 diffuse, glm::vec3 ambient) {
	for (int i = 0; i < 4; i++) {
		lights[i] = LightHandler::getInstance().createLight();

		lights[i]->specular = specular; lights[i]->diffuse = diffuse; lights[i]->ambient = ambient;
		delete lights[i]->pos; lights[i]->pos = &m_position;
		delete lights[i]->direction;

		lights[i]->arc = glm::pi<float>();
	}
	lights[0]->direction = new glm::vec3(0.0f, 1.0f, 0.0f);
	lights[1]->direction = new glm::vec3(-1.0f, 0.0f, 0.0f);
	lights[2]->direction = new glm::vec3(0.0f, -1.0f, 0.0f);
	lights[3]->direction = new glm::vec3(1.0f, 0.0f, 0.0f);
}

void Pacman::changeLight() {
	pointLight = !pointLight;	
	
	for (int i = 0; i < 4; i++) {
		lights[i]->arc = (pointLight) ? glm::pi<float>() : glm::quarter_pi<float>();
	}
}

void Pacman::update(){
	for (int i = 0; i < 4; i++)
		lights[i]->active = pointLight;

	if (m_direction != direction::NONE) lightDirection = m_direction;
	if (!pointLight)
		lights[lightDirection]->active = true;
}